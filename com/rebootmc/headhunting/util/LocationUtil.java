package com.rebootmc.headhunting.util;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;







public class LocationUtil
{
  public static String locationAsString(Location location)
  {
    return location.getWorld().getName() + ";" + location.getBlockX() + ";" + location.getBlockY() + ";" + location.getBlockZ();
  }
  







  public static Location stringAsLocation(String string)
  {
    String[] parts = string.split(";");
    
    if (parts.length >= 4) {
      World world = Bukkit.getWorld(parts[0]);
      
      if (world != null) {
        double x = Double.parseDouble(parts[1]);
        double y = Double.parseDouble(parts[2]);
        double z = Double.parseDouble(parts[3]);
        
        return new Location(world, x, y, z);
      }
    }
    
    return null;
  }
}
