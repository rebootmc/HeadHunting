package com.rebootmc.headhunting;

import java.text.NumberFormat;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.plugin.java.JavaPlugin;

public class HeadHuntingPlugin extends JavaPlugin
{
  private Economy economy;
  public static Pattern NAME_PATTERN;
  public Pattern MOB_PATTERN;
  
  public void onEnable()
  {
    saveDefaultConfig();
    getServer().getPluginManager().registerEvents(new PlayerListener(this), this);
    this.economy = ((Economy)getServer().getServicesManager().getRegistration(Economy.class).getProvider());
    
    NAME_PATTERN = Pattern.compile(ChatColor.stripColor(getMessage("settings.headFormat").replace("[player]", "([0-9a-zA-Z_]+)")));
    this.MOB_PATTERN = Pattern.compile(ChatColor.stripColor(getMessage("settings.mobFormat").replace("[mob]", "([0-9a-zA-Z_]+)")));
  }
  
  public String getMessage(String path) {
    return ChatColor.translateAlternateColorCodes('&', getConfig().getString(path));
  }
  
  public List<String> getList(String path) {
    List<String> list = new java.util.ArrayList();
    
    for (String string : getConfig().getStringList(path)) {
      list.add(ChatColor.translateAlternateColorCodes('&', string));
    }
    
    return list;
  }
  
  public Economy getEconomy() {
    return this.economy;
  }
  
  public String formatDouble(double d) {
    NumberFormat format = NumberFormat.getInstance(java.util.Locale.ENGLISH);
    format.setMaximumFractionDigits(2);
    format.setMinimumFractionDigits(0);
    return format.format(d);
  }
  
  public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
  {
    if (!(sender instanceof Player)) {
      sender.sendMessage("Only players can sell heads");
    } else if (!sender.hasPermission("headhunting.sell")) {
      sender.sendMessage(getMessage("messages.permission"));
    } else {
      Player player = (Player)sender;
      ItemStack item = player.getItemInHand();
      
      if ((args.length > 0) && (args[0].equalsIgnoreCase("all"))) {
        if (sender.hasPermission("headhunting.sell.all")) {
          int sold = 0;
          double totalMoney = 0.0D;
          for (ItemStack i : player.getInventory().getContents()) {
            if ((i != null) && (i.getType() == Material.SKULL_ITEM) && 
              (i.hasItemMeta()) && 
              (i.getItemMeta().hasLore())) {
              Matcher m = this.MOB_PATTERN.matcher(i.getItemMeta().getDisplayName());
              if (m.find()) {
                String sell = trySell(player, i);
                totalMoney += Double.parseDouble(sell.split("_")[1]);
                sold += i.getAmount();
                player.getInventory().remove(i);
              }
            }
          }
          


          player.updateInventory();
          player.sendMessage(getMessage("messages.mobHeadsSold").replace("[count]", "" + sold).replace("[total]", "" + totalMoney));
        } else {
          sender.sendMessage(getMessage("messages.permission"));
        }
        return true;
      }
      
      if ((item == null) || (item.getType() != Material.SKULL_ITEM) || (!item.hasItemMeta())) {
        sender.sendMessage(getMessage("messages.mustBeHoldingValidHead"));
      } else {
        ItemMeta meta = item.getItemMeta();
        
        if (!meta.hasDisplayName()) {
          sender.sendMessage(getMessage("messages.mustBeHoldingValidHead"));
        }
        else if (isMobHead(item)) {
          String sell = trySell(player, item);
          
          player.sendMessage(getMessage("messages.mobHeadSold")
            .replace("[count]", "" + item.getAmount())
            .replace("[mob]", retrieveName(getMessage("settings.mobFormat"), meta.getDisplayName(), "[mob]"))
            .replace("[amount]", formatDouble(getMoney(item)))
            .replace("[total]", "" + sell.split("_")[1]));
          
          player.setItemInHand(null);
        } else {
          String whosHead = retrieveName(getMessage("settings.headFormat"), meta.getDisplayName(), "[player]");
          double sellPrice = getMoney(item);
          
          if ((!Double.isInfinite(sellPrice)) && (!Double.isNaN(sellPrice)) && (sellPrice > 0.0D)) {
            this.economy.depositPlayer(player, sellPrice);
            
            if (item.getAmount() > 1) {
              item.setAmount(item.getAmount() - 1);
              player.setItemInHand(item);
            } else {
              player.setItemInHand(null);
            }
            
            player.updateInventory();
            sender.sendMessage(getMessage("messages.headSold")
              .replace("[player]", whosHead)
              .replace("[amount]", formatDouble(sellPrice)));
          }
        }
      }
    }
    
    return true;
  }
  
  public String trySell(Player player, ItemStack item) {
    double sellPrice = getMoney(item);
    
    if ((!Double.isInfinite(sellPrice)) && (!Double.isNaN(sellPrice))) {
      int total = item.getAmount();
      this.economy.depositPlayer(player, item.getAmount() * sellPrice);
      return total + "_" + total * sellPrice;
    }
    player.updateInventory();
    return null;
  }
  
  public String retrieveName(String format, String displayName, String toRemove) {
    displayName = ChatColor.stripColor(displayName);
    format = ChatColor.stripColor(format);
    return displayName.replace(format.replace(toRemove, ""), "");
  }
  
  public boolean isMobHead(ItemStack stack) {
    if ((stack.getType() == Material.SKULL_ITEM) && 
      (stack.hasItemMeta())) {
      if (stack.getDurability() != 3) return true;
      SkullMeta skullMeta = (SkullMeta)stack.getItemMeta();
      if (skullMeta.getOwner().startsWith("MHF")) { return true;
      }
    }
    
    return false;
  }
  
  public double getMoney(ItemStack item) {
    double d = -1.0D;
    
    if ((item == null) || (item.getType() != Material.SKULL_ITEM) || (!item.hasItemMeta())) {
      return d;
    }
    ItemMeta meta = item.getItemMeta();
    
    if (!meta.hasDisplayName()) {
      return d;
    }
    if (item.getItemMeta().hasLore()) {
      Matcher match = Pattern.compile("\\$([0-9]+)").matcher(((String)item.getItemMeta().getLore().get(0)).replace(",", ""));
      if (match.find()) {
        double sellPrice = Double.parseDouble(match.group(0).replace("$", ""));
        
        if ((!Double.isInfinite(sellPrice)) && (!Double.isNaN(sellPrice))) {
          return sellPrice;
        }
      }
    }
    


    return d;
  }
}
