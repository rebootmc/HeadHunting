package com.rebootmc.headhunting;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

public class PlayerListener implements org.bukkit.event.Listener {
	private HeadHuntingPlugin plugin;
	private Map<Location, HeadInformation> heads = new java.util.HashMap();
	private java.io.File headsFile;
	private YamlConfiguration headFileYaml;
	private EntityType[] defaultMobs = { EntityType.CREEPER, EntityType.ZOMBIE, EntityType.SKELETON,
			EntityType.WITHER };

	public PlayerListener(HeadHuntingPlugin plugin) {
		this.plugin = plugin;
		loadFile();
		loadHeads();
	}

	private void loadFile() {
		this.headsFile = new java.io.File(this.plugin.getDataFolder(), "storage.yml");
		if (this.headsFile.exists()) {
			try {
				this.headsFile.createNewFile();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		this.headFileYaml = YamlConfiguration.loadConfiguration(this.headsFile);
	}

	private void loadHeads() {
		for (String key : this.headFileYaml.getKeys(false)) {
			Location headLocation = com.rebootmc.headhunting.util.LocationUtil.stringAsLocation(key);
			String ownerName = this.headFileYaml.getString(key + ".skullOwner");
			double value = this.headFileYaml.getDouble(key + ".value");
			boolean isMob = this.headFileYaml.getBoolean(key + ".isMob");

			HeadInformation info = new HeadInformation(ownerName, value, isMob);
			this.heads.put(headLocation, info);
		}
	}

	private void saveFile() {
		try {
			this.headFileYaml.save(this.headsFile);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public void saveHeadLocation(Location location, HeadInformation info) {
		String key = com.rebootmc.headhunting.util.LocationUtil.locationAsString(location);
		this.headFileYaml.set(key + ".skullOwner", info.owner);
		this.headFileYaml.set(key + ".value", Double.valueOf(info.value));
		this.headFileYaml.set(key + ".isMob", Boolean.valueOf(info.isMob));
		saveFile();
	}

	public void removeHeadLocation(Location location) {
		String key = com.rebootmc.headhunting.util.LocationUtil.locationAsString(location);
		this.headFileYaml.set(key, null);
		saveFile();
	}

	@EventHandler(ignoreCancelled = true, priority = org.bukkit.event.EventPriority.HIGHEST)
	public void onBlockBreak(BlockBreakEvent event) {
		if ((event.getBlock().getType() == Material.SKULL)
				&& (this.heads.containsKey(event.getBlock().getLocation()))) {
			HeadInformation info = (HeadInformation) this.heads.remove(event.getBlock().getLocation());
			event.setCancelled(true);
			event.getBlock().setType(Material.AIR);
			ItemStack skull;
			if (info.isMob) {
				skull = createCustomMobSkull(info.owner.replace("MHF_", ""), info.value);
			} else {
				skull = createSkull(info.owner, info.value);
			}
			event.getPlayer().getWorld().dropItemNaturally(event.getBlock().getLocation(), skull);
			removeHeadLocation(event.getBlock().getLocation());
		}

		if (event.getBlock().getType() == Material.SKULL) {
			List<ItemStack> dropped = new ArrayList();
			event.setCancelled(true);
			dropped.addAll(event.getBlock().getDrops());
			event.getBlock().setType(Material.AIR);

			EntityType type = getTypeForDataValue(((ItemStack) dropped.get(0)).getDurability());
			if (type != null) {
				ItemStack skull = createDefaultMobSkull(type, getValueFor(type));
				event.getPlayer().getWorld().dropItemNaturally(event.getBlock().getLocation(), skull);
			}
		}
	}

	@EventHandler(ignoreCancelled = true, priority = org.bukkit.event.EventPriority.MONITOR)
	public void onBlockPlace(BlockPlaceEvent event) {
		ItemStack inHand = event.getItemInHand();

		if ((inHand != null) && (inHand.getType() == Material.SKULL_ITEM) && (inHand.getDurability() == 3)) {
			double value = this.plugin.getMoney(inHand);

			if (value > 0.0D) {
				String owner = ((SkullMeta) inHand.getItemMeta()).getOwner();
				HeadInformation info = new HeadInformation(owner, value, isMobHead(owner));
				this.heads.put(event.getBlockPlaced().getLocation(), info);
				saveHeadLocation(event.getBlockPlaced().getLocation(), info);
			}
		}
	}

	public boolean isMobHead(String username) {
		return username.startsWith("MHF_");
	}

	@EventHandler
	public void onPlayerDeath(org.bukkit.event.entity.PlayerDeathEvent event) {
		Player player = event.getEntity();
		Player killer = player.getKiller();

		if (!((HeadHuntingPlugin) HeadHuntingPlugin.getPlugin(HeadHuntingPlugin.class)).getConfig().getBoolean("player-drops")) {
			return;
		}
		if ((killer == null) && ((player.getLastDamageCause() instanceof EntityDamageByEntityEvent))) {
			EntityDamageByEntityEvent edbe = (EntityDamageByEntityEvent) player.getLastDamageCause();
			Entity damager = edbe.getDamager();

			if ((damager instanceof Projectile)) {
				Projectile proj = (Projectile) damager;

				if ((proj.getShooter() instanceof Player)) {
					killer = (Player) proj.getShooter();
				}
			}
		}

		if ((killer != null) && (killer != player)) {
			double balance = this.plugin.getEconomy().getBalance(player);
			int percent = -1;

			if (this.plugin.getConfig().isConfigurationSection("percents")) {
				for (String key : this.plugin.getConfig().getConfigurationSection("percents").getKeys(false)) {
					if (killer.hasPermission("headhunting." + key)) {
						percent = this.plugin.getConfig().getInt("percents." + key);
						break;
					}
				}
			}

			if (percent == -1) {
				percent = this.plugin.getConfig().getInt("settings.defaultPercent");
			}

			double withdraw = Math.floor(balance * (percent / 100.0D));

			if (withdraw > 0.0D) {
				ItemStack headItem = createSkull(player.getName(), withdraw);

				this.plugin.getEconomy().withdrawPlayer(player, withdraw);
				player.getWorld().dropItemNaturally(player.getLocation(), headItem);
			}
		}
	}

	@EventHandler
	public void onMobDeath(EntityDamageByEntityEvent event) {
		if(!(event.getDamager() instanceof Player)) {
	        return;
	    }
		
		if((((Damageable) event.getEntity()).getHealth() - event.getDamage()) > 0) {
			return;
		}
		
		if (!(event.getEntity() instanceof Player)) {
			Entity en = event.getEntity();

			if (!((HeadHuntingPlugin) HeadHuntingPlugin.getPlugin(HeadHuntingPlugin.class)).getConfig().getBoolean("mob-drops")) {
				return;
			}
			if ((en.getType() == EntityType.WITHER) || (en.getType() == EntityType.SNOWMAN) || (en.getType() == EntityType.BAT) || (en.getType() == EntityType.HORSE)) {
				return;
			}
			
			int r = getRandom(1,100);
			
			if (r <= getDropRateFor(en.getType())) {
				for (EntityType et : this.defaultMobs)
					if (en.getType().equals(et)) {
						en.getWorld().dropItemNaturally(en.getLocation(), createDefaultMobSkull(en.getType(), getValueFor(en.getType())));
						return;
					}
				String mobName;
				switch (en.getType()) {
				case MUSHROOM_COW:
					mobName = "MushroomCow";
					break;
				case CAVE_SPIDER:
					mobName = "CaveSpider";
					break;
				case MAGMA_CUBE:
					mobName = "LavaSlime";
					break;
				case PIG_ZOMBIE:
					mobName = "PigZombie";
					break;
				case IRON_GOLEM:
					mobName = "Golem";
					break;
				default:
					mobName = org.apache.commons.lang.WordUtils.capitalize(en.getType().name().toLowerCase());
				}
				en.getWorld().dropItemNaturally(en.getLocation(), createCustomMobSkull(mobName, getValueFor(en.getType())));
			}
			
			
		}
	}

	@EventHandler
	public void onPickup(PlayerPickupItemEvent event) {
		if (event.getItem().getItemStack().getType() == Material.SKULL_ITEM) {
			event.getPlayer().updateInventory();
		}
	}

	private ItemStack createDefaultMobSkull(EntityType type, double value) {
		int data = 0;
		String name = "";

		switch (type) {
		case SKELETON:
			data = 0;
			name = "Skeleton";
			break;
		case ZOMBIE:
			data = 2;
			name = "Zombie";
			break;
		case CREEPER:
			data = 4;
			name = "Creeper";
		}

		ItemStack head = new ItemStack(Material.SKULL_ITEM);
		head.setDurability((short) data);
		org.bukkit.inventory.meta.ItemMeta meta = head.getItemMeta();
		meta.setDisplayName(this.plugin.getMessage("settings.mobFormat").replace("[mob]", name).replace("[money]", "$" + this.plugin.formatDouble(value)));

		List<String> parsed = new ArrayList();
		for (String str : this.plugin.getList("settings.headLore")) {
			if (str.contains("[money]"))
				parsed.add(str.replace("[money]", "$" + this.plugin.formatDouble(value)));
			else
				parsed.add(str);
		}
		meta.setLore(parsed);

		head.setItemMeta(meta);

		return head;
	}
	
	public int getRandom(int lower, int upper) {
	      Random random = new Random();
	      return random.nextInt((upper - lower) + 1) + lower;
	  }

	private ItemStack createCustomMobSkull(String mobName, double value) {
		ItemStack head = new ItemStack(Material.SKULL_ITEM);
		head.setDurability((short) 3);
		SkullMeta meta = (SkullMeta) head.getItemMeta();
		meta.setOwner("MHF_" + mobName);
		meta.setDisplayName(this.plugin.getMessage("settings.mobFormat").replace("[mob]", mobName).replace("[money]",
				"$" + this.plugin.formatDouble(value)));

		List<String> parsed = new ArrayList();
		for (String str : this.plugin.getList("settings.headLore")) {
			if (str.contains("[money]"))
				parsed.add(str.replace("[money]", "$" + this.plugin.formatDouble(value)));
			else
				parsed.add(str);
		}
		meta.setLore(parsed);

		head.setItemMeta(meta);

		return head;
	}

	private ItemStack createSkull(String ownerName, double value) {
		ItemStack headItem = new ItemStack(Material.SKULL_ITEM);
		headItem.setDurability((short) 3);
		SkullMeta meta = (SkullMeta) headItem.getItemMeta();
		meta.setOwner(ownerName);
		meta.setDisplayName(this.plugin.getMessage("settings.headFormat").replace("[player]", ownerName)
				.replace("[money]", "$" + this.plugin.formatDouble(value)));

		List<String> parsed = new ArrayList();
		for (String str : this.plugin.getList("settings.headLore")) {
			if (str.contains("[money]"))
				parsed.add(str.replace("[money]", "$" + this.plugin.formatDouble(value)));
			else
				parsed.add(str);
		}
		meta.setLore(parsed);

		headItem.setItemMeta(meta);
		return headItem;
	}

	public double getValueFor(EntityType type) {
		if (this.plugin.getConfig().get("mobs." + type.name().toLowerCase()) != null) {
			return this.plugin.getConfig().getDouble("mobs." + type.name().toLowerCase());
		}
			
		return this.plugin.getConfig().getDouble("mobs.default");
	}
	
	public double getDropRateFor(EntityType type) {
		if (this.plugin.getConfig().get("droprate." + type.name().toLowerCase()) != null) {
			return this.plugin.getConfig().getDouble("droprate." + type.name().toLowerCase());
		}
			
		return this.plugin.getConfig().getDouble("droprate.default");
	}

	public EntityType getTypeForDataValue(int value) {
		switch (value) {
		case 0:
			return EntityType.SKELETON;
		case 2:
			return EntityType.ZOMBIE;
		case 4:
			return EntityType.CREEPER;
		}
		return null;
	}

	public static class HeadInformation {
		private String owner;

		private double value;
		private boolean isMob;

		public HeadInformation(String owner, double value, boolean isMob) {
			this.owner = owner;
			this.value = value;
			this.isMob = isMob;
		}
	}
}
